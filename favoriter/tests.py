from django.test import TestCase, Client
from django.utils import timezone
from django.urls import reverse

from users.models import CustomUser
from .models import FavoriteList, Song, FavoriteRegister, Artist, ReleaseYear, Album


#  in PSQL must have add CREATEDB permissions to database user:
#ALTER USER {username} CREATEDB;

# test user login
def create_user(username, password):
    user = CustomUser.objects.create(username=username)
    user.set_password(password)
    user.save()
    return user

class UserLoginTests(TestCase):
    """
        Basic Login Tests
    """
    def test_login(self):
        self.user = create_user('testuser', 'pass1234')
        logged_in = self.client.login(username='testuser', password='pass1234')
        self.assertTrue(logged_in)

    def test_login_fail_username(self):
        self.user = create_user('testuser', 'pass1234')
        logged_in = self.client.login(username='testuser3', password='pass1234')
        self.assertFalse(logged_in)

    def test_login_fail_pw(self):
        self.user = create_user('testuser', 'pass1234')
        logged_in = self.client.login(username='testuser', password='pass12345')
        self.assertFalse(logged_in)

class FavoriteListAutoCreationTest(TestCase):
    def test_personal_favorite_list_made_on_user_creation(self):
        """
        Test whether a favoriteList is automatically created upon a
        new user being created
        """
        self.user = create_user('testuser', 'pass1234')
        user_default_list = self.user.favoritelist_set.first()
        self.assertEqual(user_default_list.user, self.user)


# check add_or_update_register
    # check won't add songs if not logged in
    # check adding songs with weird data
    # check default with showid / that showid is most recent
    # check that it updates timezone to most recent

# check add_data_from_playid
    # Check each data type and that it's created correctly
        # added, null, or already created

# check ajax_add_song

# check ajax_change_register_status

#feed view
    # make sure feed_list looks correct

# favorite_list view
    # check context if user not authenticated
    # make sure register sets look ok
