from django.db import models
from django.utils import timezone
from users.models import CustomUser

# Use signals to automatically create a favorite list for a new user
def create_favorite_list(sender, instance, created, **kwargs):
    if created:
        FavoriteList.objects.create(name=instance.username + '\'s Favorites', user=instance)

models.signals.post_save.connect(create_favorite_list, sender=CustomUser, weak=False, dispatch_uid='models.create_favorite_list')


class Artist(models.Model):
    name = models.CharField(max_length=500)
    kexp_artistid = models.PositiveIntegerField(null=True, blank=True)

    def __str__(self):
        return self.name

class ReleaseYear(models.Model):
    year = models.PositiveIntegerField(null=True, blank=True)

    def __str__(self):
        return str(self.year)

class Album(models.Model):
    name = models.CharField(max_length=600)
    kexp_releaseid = models.PositiveIntegerField(null=True, blank=True)
    artist = models.ForeignKey('Artist', on_delete=models.CASCADE)
    year = models.ForeignKey('ReleaseYear', on_delete=models.SET_NULL, null=True, blank=True)
    label = models.CharField(max_length=600, null=True, blank=True)
    small_image_uri = models.URLField(null=True, blank=True)
    large_image_uri = models.URLField(null=True, blank=True)

    def __str__(self):
        return self.name

class Song(models.Model):
    name = models.CharField(max_length=600)
    kexp_trackid = models.PositiveIntegerField(null=True, blank=True)
    artist = models.ForeignKey('Artist', on_delete=models.CASCADE)
    album = models.ForeignKey('Album', on_delete=models.SET_NULL, blank=True, null=True)
    year = models.ForeignKey('ReleaseYear', on_delete=models.SET_NULL, null=True, blank=True)

    def __str__(self):
        return self.name

class FavoriteList(models.Model):
    name = models.CharField(max_length=200)
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE, null=True)
    songlist = models.ManyToManyField(
        'Song',
        through = 'FavoriteRegister',
        through_fields = ('favorite_list', 'song'),
        related_name='songs',
    )

    def __str__(self):
        return self.name

class FavoriteRegister(models.Model):
    favorite_list = models.ForeignKey(FavoriteList, on_delete=models.CASCADE)
    song = models.ForeignKey(Song, on_delete=models.CASCADE)
    date_added = models.DateTimeField('when added', default=timezone.now)
    is_active = models.BooleanField(default=True)
    show_added_id = models.PositiveIntegerField(null=True, blank=True)
    notes = models.CharField(max_length=600, blank=True)

    def __str__(self):
        return self.favorite_list.name + ': ' + self.song.name
