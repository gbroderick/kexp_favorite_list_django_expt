from django.contrib import admin


from .models import Song, Album, Artist, FavoriteList, FavoriteRegister, ReleaseYear

admin.site.register(ReleaseYear)
admin.site.register(Artist)
admin.site.register(Album)
admin.site.register(Song)
admin.site.register(FavoriteList)
admin.site.register(FavoriteRegister)
