from django.urls import path

from . import views

urlpatterns = [
    path('', views.feed, name='feed'),
    path('favorite-list/', views.favorite_list, name='favorite-list'),
    path('ajax/change_song_status/', views.ajax_change_register_status, name='ajax_change_song_status'),
    path('ajax/add_song/', views.ajax_add_song, name='ajax_add_song'),
]
