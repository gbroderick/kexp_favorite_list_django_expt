from django.apps import AppConfig


class FavoriterConfig(AppConfig):
    name = 'favoriter'
