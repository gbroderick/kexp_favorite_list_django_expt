# Generated by Django 2.1.7 on 2019-03-22 18:26

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('favoriter', '0002_auto_20190322_0428'),
    ]

    operations = [
        migrations.RenameField(
            model_name='album',
            old_name='title',
            new_name='name',
        ),
        migrations.RenameField(
            model_name='song',
            old_name='title',
            new_name='name',
        ),
    ]
