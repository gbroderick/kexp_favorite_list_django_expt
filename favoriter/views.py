import json, requests
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.urls import reverse
from django.utils import timezone
from django.contrib.staticfiles.templatetags.staticfiles import static
# For using test data
from django.conf import settings
import os

from .models import FavoriteList, Song, FavoriteRegister, Artist, ReleaseYear, Album
from django.core.files import File


def feed(request):
    use_test_data = False
    if settings.DEBUG and use_test_data:
        with open(os.path.join('favoriter/static/favoriter/samplePlayData.json')) as f:
            init_data = json.load(f)
        data = init_data
    else:
        resp = requests.get('http://128.208.196.80/play/')
        data = resp.json()
    # TODO better error checking and handling on data['results']
    data_parsed = [x for x in data['results'] if x['playtype']['playtypeid'] == 1]

    if request.user.is_authenticated:
        data_register_list = []
        for song_data in data_parsed:
            if song_data['artist'] and song_data['track']:
                match = FavoriteRegister.objects.filter(
                    favorite_list__user=request.user,
                    song__artist__name = song_data['artist']['name'],
                    song__name = song_data['track']['name'],
                )
                if match.count() == 0:
                    song_data['registered'] = False
                    song_data['register_active'] = False
                else:
                    song_data['registered'] = True
                    song_data['register_active'] = match.values_list('is_active')[0][0]
                    song_data['register_id'] = match.values_list('id')[0][0]
                data_register_list.append(song_data)
        data_parsed = data_register_list

    context = {
        'feed_list': data_parsed,
        'backup_album_image': static('favoriter/images/record.svg')
    }
    return render(request, 'favoriter/feed.html', context)

def favorite_list(request):
    context = {
        'backup_album_image': static('favoriter/images/record.svg')
    }
    if request.user.is_authenticated:
        favorite_list = FavoriteList.objects.filter(user=request.user).first()
        register_set = favorite_list.favoriteregister_set.filter(is_active=True).order_by('date_added').reverse()
        context['favorite_list'] = favorite_list
        context['register_set'] = register_set

    return render(request, 'favoriter/list.html', context)

# Ajax/DB update views
def add_data_from_playid(playid):
    resp = requests.get('http://128.208.196.80/play/?playid=' + playid)
    play_data = resp.json()
    data = play_data['results'][0]
    if data['playtype']['playtypeid'] == 1:
        artist, artist_status = Artist.objects.get_or_create(
            kexp_artistid=data['artist']['artistid'],
            name=data['artist']['name'],
        )
        if data['releaseevent']:
            year, year_status = ReleaseYear.objects.get_or_create(year=data['releaseevent']['year'])
        else:
            year = None
        if data['release']:
            album, album_status = Album.objects.get_or_create(
                name= data['release']['name'],
                artist = artist,
                year = year,
            )
            #update album info for repeat appearance
            album.small_image_uri = data['release']['smallimageuri']
            album.large_image_uri = data['release']['largeimageuri']
            album.label =  data['label']['name'] if data['label'] else None
            album.kexp_releaseid = data['release']['releaseid']
            album.save()
        else:
            album = None
        song, songstatus = Song.objects.get_or_create(
            name= data['track']['name'],
            artist=artist,
            album=album,
            year=year,
        )
        #update track id for repeat appearance
        song.kexp_trackid= data['track']['trackid']
        song.save()

        return song
    else:
        return None

def add_or_update_register(song, user, showid):
    if user.is_authenticated:
        favorite_list = user.favoritelist_set.first()
        register, registerstatus = FavoriteRegister.objects.get_or_create(
            favorite_list = user.favoritelist_set.first(),
            song = song,
        )
        register.show_added_id = showid
        register.date_added = timezone.now()
        register.save()
        return register

def ajax_add_song(request):
    response = {'response': '',}
    if request.is_ajax():
        trackid = request.POST.get('trackid')
        playid = request.POST.get('playid')
        showid = request.POST.get('showid')
        try:
            song = Song.objects.get(kexp_trackid=trackid)
        except (KeyError, Song.DoesNotExist):
            song = add_data_from_playid(playid)
            if not song:
                response['error'] += "error: playid: " + playid + " is invalid."
                return JsonResponse(response)
        register = add_or_update_register(song, request.user, showid)
        response['id'] = register.id
    return JsonResponse(response)

def ajax_change_register_status(request):
    data = {'response': '',}
    if request.is_ajax():
        registerid = request.POST.get('registerid')
        try:
            list = FavoriteRegister.objects.get(pk=registerid)
        except (KeyError, FavoriteRegister.DoesNotExist):
            data['response'] = "Key Does Not Exist"
            return JsonResponse(data)

        # primitive check to see if songname and id songname agree?
        if list.favorite_list.user == request.user:
            list.is_active = not list.is_active;
            list.save()
            data['response'] = "updated: " + list.__str__()
        else:
            data['response'] = "invalid user"
    return JsonResponse(data)


def song_details(request, song_id):
    return HttpResponse("this is detail for the song %s" % song_id)
