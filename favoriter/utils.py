import json, requests


def kexp_playitem_restructure(data_playitem):
    new_playitem = dict()
    new_playitem['show_added_id'] = data_playitem['showid']
    new_playitem['song'] = {
        'title': data_playitem['track']['name'],
        'kexp_trackid': data_playitem['track']['trackid'],
    }

    if data_playitem['artist']:
        new_playitem['artist'] = {
            'name': data_playitem['artist']['name'],
            'kexp_artistid': data_playitem['artist']['artistid'],
        }
    else:
        new_playitem['artist'] = None

    if data_playitem['release']:
        new_playitem['album'] = {
            'title': data_playitem['release']['name'],
            'kexp_releaseid': data_playitem['release']['releaseid'],
            'small_image_uri': data_playitem['release']['smallimageuri'],
            'large_image_uri': data_playitem['release']['largeimageuri'],
        }
    else:
        new_playitem['album'] = None

    if data_playitem['label']:
        new_playitem['album']['label'] = data_playitem['label']['name']
    else:
        new_playitem['album']['label'] = None

    if data_playitem['releaseevent']:
        new_playitem['release_year'] = data_playitem['releaseevent']['year']
    else:
        new_playitem['release_year'] = None
        
    #List of Dicts into new Dicts reflecting the Models
    #Artist / Album / Song / Year
    #show added as vestigial
    return new_playitem

def kexp_parse_request(data_dict):
    play_list = data_dict.get('results')
    play_list_media = list();
    for result in play_list:
        if result['playtype']['playtypeid'] == 1:
            result_restructure = kexp_playitem_restructure(result)
            play_list_media.append(result_restructure)
            # play_list_media.append(result)

    return play_list_media


def json_request(url):
    resp = requests.get(url)
    # if resp.codes.ok
    data = resp.json()
    return data
