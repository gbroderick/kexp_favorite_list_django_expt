#Favorite List App Experiment using KEXP's Feed

A simple Django/Postgres web app to create user-based favorite lists using the [Playlist Feed](http://128.208.196.80/) of [KEXP](https://kexp.org/)

---

##Local Setup

Make sure you have Python (preferably Python3 with Pip), Postgres, and Node installed on your machine, all of which should be available with [Homebrew](https://brew.sh/). If using a Mac, [this walkthrough](https://medium.com/@briantorresgil/definitive-guide-to-python-on-mac-osx-65acd8d969d0) was extremely helpful in setting up Python as well as next section on virtual environments.

###Virtual Environment Setup and OS variables
If you haven't already, set up a virtual environment for handling OS variables and installing Python packages non-globally. There are a few ways to setup a virtual environment for Python. Some folks have suggested using [direnv](https://direnv.net/), but I personally am using virtualenv and [virtualenvwrapper](https://virtualenvwrapper.readthedocs.io/en/latest/#).

Using venvwrapper, create a virtual environment with:
```
mkvirtualenv -p python3 project_name
```
To get out of the virtual environment, simply type `deactivate`. To activate and work within it again, `workon project_name`.

Set a directory for the Virtual Environment to automatically go to on activation by working on the virtual environment, and in the case of the current working directory, enter in:
```
setvirtualenvproject $VIRTUAL_ENV .
```

####Setting the necessary OS variables for the virtual environment

Inside your virtual environment, it's necessary to set up several variables to ensure Django has access to the Postgres database credentials and the crypto key without exposing them. You can set them once inside your virtual environment by editing its `postactivate` and `predeactivate` scripts.

You'll want to come up with a database name, user, and password for that user before this step, if you haven't already set up a PSQL database.

Edit `$VIRTUAL_ENV/bin/postactivate` to include:
```
export DB_NAME=database_name
export DB_USER=database_user
export DB_PASSWORD=database_password
export DJANGO_SECRET_KEY='fp3xwka$0_vlq=9velgupm3@bl0kujs3**5a!=2wm-8n^)qy1j'

```
Please note this is using a sample SECRET_KEY suitable only for development use. If moving into production replace this with a new secret key

Also edit  `$VIRTUAL_ENV/bin/predeactivate` to include:

```
unset DB_NAME
unset DB_USER
unset DB_PASSWORD
unset DJANGO_SECRET_KEY

```


###Postgres Setup

Enter the Postgres CLI with
```
psql
```
Then, using the same database name, user, and password titles entered into the postactivate script above, create the user:

```
CREATE USER database_user WITH PASSWORD 'database_password';
```

And the database:
```
CREATE DATABASE database_name WITH OWNER database_user;
```

Also, give the database user permission to create test databases in order to run tests:
```
ALTER USER database_user CREATEDB;
```
See Django setup for information on then migrating model information to the DB.

###Python Dependency Setup
Inside your virtual environment, install Python dependencies:
```
pip3 install -r requirements.txt
```

If new dependencies are added, save with:
```
pip3 freeze > requirements.txt

```
Note these could simply be `pip` instead of `pip3` depending on how python is set up on your machine.


###Gulp Front-End

This project uses the gulp pre-processor system to process Sass to CSS and minify/Concat JS for production.

####Installation
Install Node dependencies by going to the project directory and entering:
```
npm install
```
This should install all Node packages outlined in `package.json`. You may need to change your Node version to `8.11.3` via [NVM (node Version Manager)](https://github.com/creationix/nvm).

####Gulp Usage

The default action, `gulp`, will process files from `./gulp-src` into a debuggable dev version into its output directory, in this case `./favoriter/static/favoriter/` and start a watch on the src directory. Stop this watch at any time with ctrl + c.

`gulp --out production` will output a production-ready file from `./gulp-src` to `./production`. This should minify code without sourcemaps and so is not recommended for debugging.

---

##Django Usage
###Setup and Run Django
  - If in development, change the `DEBUG` variable in `kexp-favoriter/settings.py` to `True`.
  - Migrate your new database with `python3 manage.py makemigrations` followed by `python3 manage.py migrate`
  - create an admin with `python3 manage.py createsuperuser`
  - Run the development server with `python3 manage.py runserver`
  - Barring any issues, the app should now be running on port 8000, [http://localhost:8000/]
  - The admin panel is available in the Django default location at [http://localhost:8000/admin/]

###Useful Django Commands
  - Running the server: `python3 manage.py runserver`
  - Enter the interactive shell: `python3 manage.py shell`
  - Apply model updates `python3 manage.py makemigrations` followed by `python3 manage.py migrate`
  - Run tests `python3 manage.py tests`

###Use of Requests library and AJAX
This app makes fairly heavy use of processing AJAX requests for most of its functions. The [playlist feed](http://128.208.196.80/) is processed in views via the [Python Requests package](http://docs.python-requests.org/en/master/). Changes to the database are posted via [jQuery Ajax POST requests](http://api.jquery.com/jquery.ajax/) to specific view URLS.

---

##Heroku Deployment

Deployed at [https://peaceful-escarpment-55443.herokuapp.com/]. `requirements.txt` and the current `settings.py` are mostly set up to deploy on Heroku given the huge help of the `django-heroku` package. Once deployed, the Heroku Postgres add-on must be activated.
