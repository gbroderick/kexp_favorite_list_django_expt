//Like Button Handling for KEXP Favoriter

// Interacts with a button with the following data-attributes
// <button type="button" class="btn-box songlist-button" name="song-post-button"
//   data-song-id="1356237"
//   data-play-id="2636011"
//   data-show-id="103349"
//   data-registered="false"
//   data-register-active="false"
//   data-register-id=""
//   onclick="handleSongAjax(event, '/ajax/add_song/', '/ajax/change_song_status/')">
// </button>


var toggleCooldown = false;
var buttonMessages = {
  toAdd: '<span class="heart-container heart-neutral"><span class="heart"></span></span> Add to your list',
  added: '<span class="heart-container heart-plus"><span class="heart"></span></span> On your list',
  toAddBack: '<span class="heart-container heart-minus"><span class="heart"></span></span> Add back'
}

function handleSongAjax(e, ajaxAddSongUrl, ajaxChangeStatusUrl) {
  e.preventDefault();
  var registerStatus = e.target.getAttribute("data-registered");

  // POST ajaxAddSongUrl e.g. '/ajax/add_song/'
  // Add Song
  if (!toggleCooldown && registerStatus == 'false') {
    var trackid = e.target.getAttribute("data-song-id");
    var playid = e.target.getAttribute("data-play-id");
    var showid = e.target.getAttribute("data-show-id");

    $.ajax({
      url: ajaxAddSongUrl,
      data: {
        'trackid': trackid,
        'playid' : playid,
        'showid' : showid
      },
      dataType: 'json',
      type: 'POST',
      success: function(data) {
        e.target.setAttribute('data-registered', 'true');
        e.target.setAttribute('data-register-active', 'true');
        e.target.setAttribute("data-register-id", data.id);
        e.target.innerHTML = buttonMessages.added;

        // console.log(data.error);
        //add error
      }
    });
    toggleCooldown = true;
    window.setTimeout(function(){toggleCooldown = false;}, 500)
  }
  // POST ajaxChangeStatusUrl e.g. '/ajax/change_song_status/'
  // Toggle Registered status of registerd song
  else if (!toggleCooldown && registerStatus){
    var registerid = e.target.getAttribute("data-register-id");
    var registerActive = e.target.getAttribute('data-register-active');
    $.ajax({
      url: ajaxChangeStatusUrl,
      data: {
        'registerid': registerid
      },
      dataType: 'json',
      type: 'POST',
      success: function(data) {
        if (registerActive == 'false') {
          e.target.setAttribute('data-register-active', 'true');
          e.target.innerHTML = buttonMessages.added;
        }
        else {
          e.target.setAttribute('data-register-active', 'false');
          e.target.innerHTML = buttonMessages.toAddBack;
        }
        // console.log(data.error);
        //add error
      }
    });
    toggleCooldown = true;
    window.setTimeout(function(){toggleCooldown = false;}, 200)
  }
}
